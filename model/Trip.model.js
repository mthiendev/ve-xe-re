const { DataTypes, INTEGER } = require('sequelize');
const { Station, Seats } = require('./index');
const createTripModel = (sequelize) => {
  return sequelize.define(
    'Trip',
    {
      fromStation: {
        type: DataTypes.INTEGER,
        references: {
          model: 'stations',
          key: 'id',
        },
      },
      toStation: {
        type: DataTypes.INTEGER,
        references: {
          model: 'stations',
          key: 'id',
        },
      },
      amountSeats: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },

      startTime: {
        type: DataTypes.DATE,
        allowNull: false,
      },

      price: { type: DataTypes.FLOAT },
    },
    {
      tableName: 'trips', //
      timestamps: false, // tắt createdAt updateAt
    },
  );
};
module.exports = {
  createTripModel,
};
