const { DataTypes } = require('sequelize');
const createSeatModel = (sequelize) => {
  return sequelize.define(
    'Seat',
    {
      code: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      isBooked: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      trip_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        // references: {
        //   model: 'trips',
        //   key: 'id',
        // },
      },
    },
    {
      tableName: 'seats', //
      timestamps: false, // tắt createdAt updateAt
    },
  );
};
module.exports = {
  createSeatModel,
};
