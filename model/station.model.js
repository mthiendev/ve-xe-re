const { DataTypes } = require('sequelize');
const createSationModel = (sequelize) => {
  return sequelize.define(
    'Station',
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      address: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      province: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      tableName: 'stations', //
      timestamps: true, // tắt createdAt updateAt
    },
  );
};
module.exports = {
  createSationModel,
};
