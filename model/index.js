const { Sequelize } = require('sequelize');
const { DB, HOST, USER, PASSWORD, dialect } = require('../configs/db.configs');
const { createSeatModel } = require('./seat.model');
const { createSationModel } = require('./station.model');
const { createTripModel } = require('./Trip.model');
const sequelize = new Sequelize(DB, USER, PASSWORD, {
  host: HOST,
  dialect,
});
const Station = createSationModel(sequelize);
const Seats = createSeatModel(sequelize);
const Trips = createTripModel(sequelize);
Trips.hasMany(Seats, { foreignKey: 'trip_id' });
module.exports = {
  sequelize,
  Station,
  Seats,
  Trips,
};
