const express = require('express');
const app = express();
const port = 3000;
const api = require('./routes/api');
app.use(express.json());

app.use('/', api);
app.listen(port, () => {
  console.log(`Server runing on port ${port}`);
});

//setup sequelize
const { sequelize } = require('./model');
sequelize.sync({ alter: true });
const checkConnect = async () => {
  try {
    await sequelize.authenticate();
    console.log('kết nối thành công');
  } catch (error) {
    console.log(' kết nối thất bại');
    console.log('error: ', error);
  }
};
checkConnect();
