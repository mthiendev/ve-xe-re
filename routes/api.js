const express = require('express');
const router = express.Router();
const stationRouter = require('./controllers/stations/index');
const tripRouter = require('./controllers/trips/index');
const seatRouter = require('./controllers/Seats/index');

router.use('/api/stations', stationRouter);
router.use('/api/trips', tripRouter);
router.use('/api/seats', seatRouter);
module.exports = router;
