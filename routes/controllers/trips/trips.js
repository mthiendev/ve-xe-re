const { getSeatsBytripId } = require('../../services/seat.service');
const {
  create,
  getList,
  getDetail,
  deleteByid,
  update,
} = require('../../services/trip.service');

const seatCodes = [
  'A01',
  'A02',
  'A03',
  'A04',
  'A05',
  'A06',
  'A07',
  'A08',
  'A09',
  'A10',
  'A11',
  'A12',
  'B01',
  'B02',
  'B03',
  'B04',
  'B05',
  'B06',
  'B07',
  'B08',
  'B09',
  'B10',
  'B11',
  'B12',
];
module.exports.createTrip = async (req, res) => {
  const trip = req.body;
  const newTrip = await create(trip);
  res.status(200).send(newTrip);
};
module.exports.getTrips = async (req, res) => {
  const trips = await getList();
  if (trips) {
    res.status(200).send(trips);
  } else {
    res.status(500).send('not found');
  }
};
module.exports.getTripByID = async (req, res) => {
  const { id } = req.params;
  const trip = await getDetail(id);
  const seatsOfTrip = await getSeatsBytripId(id);
  const respon = { ...trip.dataValues, seats: seatsOfTrip };
  if (trip) {
    res.status(200).send(respon);
  } else {
    res.status(500).send('not found');
  }
};
module.exports.deleteTripByID = async (req, res) => {
  const { id } = req.params;

  const tripDelete = await deleteByid(id);
  if (tripDelete) {
    res.status(200).send(tripDelete);
  } else {
    res.status(500).send('not found');
  }
};
module.exports.updateTripByID = async (req, res) => {
  const { id } = req.params;
  const trip = req.body;

  const tripUpdate = await update(id, trip);
  if (tripUpdate) {
    res.status(200).send(tripUpdate);
  } else {
    res.status(404).send('Not found');
  }
};
