const express = require('express');
const router = express.Router();
const tripController = require('./trips');
router.post('/', tripController.createTrip);
router.get('/', tripController.getTrips);
router.get('/:id', tripController.getTripByID);
router.put('/:id', tripController.updateTripByID);
router.delete('/:id', tripController.deleteTripByID);

module.exports = router;
