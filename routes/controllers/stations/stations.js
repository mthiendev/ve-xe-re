const {
  getList,
  getDetail,
  create,
  update,
  deleteByid,
} = require('../../services/station.service');
module.exports.createStation = async (req, res, next) => {
  const station = req.body;
  const newStation = await create(station);
  res.status(200).send(newStation);
};
module.exports.getStations = async (req, res) => {
  const stations = await getList();
  if (stations) {
    res.status(200).send(stations);
  } else {
    res.status(404).send('not found');
  }
};
module.exports.getStationById = async (req, res) => {
  const { id } = req.params;
  const station = await getDetail(id);
  if (station) {
    res.status(200).send(station);
  } else {
    res.status(404).send('Not found');
  }
};
module.exports.updateStationById = async (req, res) => {
  const { id } = req.params;
  const station = req.body;
  const stationUpdate = await update(id, station);
  if (stationUpdate) {
    res.status(200).send(stationUpdate);
  } else {
    res.status(404).send('Not found');
  }
};
module.exports.deleteStationById = async (req, res) => {
  const { id } = req.params;
  const station = await deleteByid(id);
  if (station) {
    res.status(200).send(station);
  } else {
    res.status(404).send('Not found');
  }
};
