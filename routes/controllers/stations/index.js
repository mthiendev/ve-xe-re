const express = require('express');
const router = express.Router();
const stationController = require('./stations');
router.post('/', stationController.createStation);
router.get('/', stationController.getStations);
router.get('/:id', stationController.getStationById);
router.put('/:id', stationController.updateStationById);
router.delete('/:id', stationController.deleteStationById);

module.exports = router;
