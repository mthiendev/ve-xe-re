const { create, deleteSeatByID } = require('../../services/seat.service');
module.exports.creatSeat = async (req, res) => {
  const seat = req.body;
  const newSeat = await create(seat);
  res.status(200).send(newSeat);
};
module.exports.deleteSeat = async (req, res) => {
  const { id } = req.params;
  const seatDelete = await deleteSeatByID(id);
  if (seatDelete) {
    res.status(200).send(seatDelete);
  }
  res.status(500).send('notfound');
};
