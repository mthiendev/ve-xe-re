const express = require('express');
const router = express.Router();
const seatController = require('./seats');
// router.post('/', tripController.createTrip);
// router.get('/', tripController.getTrips);
// router.get('/:id', tripController.getTripById);
// router.put('/:id', tripController.updateTripById);
router.delete('/:id', seatController.deleteSeat);

module.exports = router;
