const { Trips } = require('../../model');

const { createListSeat } = require('./seat.service');
const getList = async () => {
  const tripList = await Trips.findAll();
  if (tripList) return tripList;
  else return false;
};
const getDetail = async (id) => {
  const trip = await Trips.findOne({
    where: {
      id,
    },
  });
  if (trip) return trip;
  else return false;
};
const create = async (trip) => {
  const newtrip = await Trips.create(trip);
  console.log('amountSeats', newtrip.amountSeats);
  console.log('id', newtrip.id);
  const seats = await createListSeat(newtrip.amountSeats, newtrip.id);
  const res = { ...newtrip.dataValues };
  if (newtrip) {
    return res;
  }
  return false;
};
const update = async (id, trip) => {
  const tripUpdate = await getDetail(id);
  console.log('tripUpdate: ', tripUpdate);

  if (tripUpdate) {
    await tripUpdate.update({
      fromStation: trip.fromStation,
      toStation: trip.toStation,
      startTime: trip.startTime,
      price: trip.price,
    });

    return tripUpdate;
  }
  return false;
};
const deleteByid = async (id) => {
  const tripDelete = await getDetail(id);

  if (tripDelete) {
    await Trips.destroy({
      where: {
        id,
      },
    });
    return tripDelete;
  } else {
    return false;
  }
};
module.exports = {
  getList,
  getDetail,
  create,
  update,
  deleteByid,
};
