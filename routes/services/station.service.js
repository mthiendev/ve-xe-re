const { Station } = require('../../model');
const getList = async () => {
  const stationList = await Station.findAll();
  if (stationList) return stationList;
  else return false;
};
const getDetail = async (id) => {
  const station = await Station.findOne({
    where: {
      id,
    },
  });
  if (station) return station;
  else return false;
};
const create = async (station) => {
  const newStation = await Station.create(station);
  if (newStation) {
    return newStation;
  }
  return false;
};
const update = async (id, station) => {
  const stationUpdate = await getDetail(id);
  console.log('stationUpdate: ', stationUpdate);

  if (stationUpdate) {
    stationUpdate.name = station.name;
    stationUpdate.address = station.address;
    stationUpdate.province = station.province;
    await stationUpdate.save();
    return stationUpdate;
  }
  return false;
};
const deleteByid = async (id) => {
  const stationDelete = await getDetail(id);
  if (stationDelete) {
    await Station.destroy({
      where: {
        id,
      },
    });
    return stationDelete;
  } else {
    return false;
  }
};
module.exports = {
  getList,
  getDetail,
  create,
  update,
  deleteByid,
};
