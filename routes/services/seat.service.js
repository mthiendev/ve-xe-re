const { Seats } = require('../../model');
const createSeat = async (seat) => {
  const newSeat = await Seats.create(seat);
  if (newSeat) {
    return newSeat;
  }
  return false;
};
const createListSeat = async (amountSeats, trip_id) => {
  const seats = [];
  for (let i = 1; i <= amountSeats + 1; i++) {
    const seat = { code: i, trip_id };
    const newSeat = await createSeat(seat);
    if (!newSeat) {
      return false;
    } else {
      seats.push(newSeat);
    }
  }
  return seats;
};
const getSeatsBytripId = async (trip_id) => {
  const seats = await Seats.findAll({
    where: {
      trip_id,
    },
  });
  if (seats) return seats;
  return false;
};
const getDetail = async (id) => {
  const seat = await Seats.findAll({
    where: { id },
  });
  if (seat) return seat;
  return false;
};
const deleteSeatByID = async (id) => {
  const seatDelete = await getDetail(id);
  if (seatDelete) {
    await Seats.destroy({
      where: {
        id,
      },
    });
    return seatDelete;
  } else {
    return false;
  }
};

module.exports = {
  createListSeat,
  createSeat,
  deleteSeatByID,
  getSeatsBytripId,
};
